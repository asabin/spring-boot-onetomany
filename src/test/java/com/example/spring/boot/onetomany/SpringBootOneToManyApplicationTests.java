package com.example.spring.boot.onetomany;

import com.example.spring.boot.onetomany.entity.Company;
import com.example.spring.boot.onetomany.entity.Project;
import com.example.spring.boot.onetomany.service.CompanyService;
import com.example.spring.boot.onetomany.service.ProjectService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpringBootOneToManyApplication.class)
public class SpringBootOneToManyApplicationTests {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private ProjectService projectService;

    // These all work without different annotation if foreign key of project has on update = CASCADE, on delete = CASCADE
    @Test
    public void test() {
        testCompany();
        testProject();
        testDeleteCompanyWithProject();
    }

//    @Test
    public void testCompany() {
        testSaveCompany();
        testFindCompanyById();
        testFindAllCompanies();
        testDeleteCompany();
        testExistCompany();
        testFindCompanyByName();
    }

//    @Test
    public void testProject() {
        testSaveProject();
        testFindProjectById();
        testFindAllProjects();
        testDeleteProject();
        testExistProject();
        testFindProjectByName();
    }
    
//    @Test
    public void testSaveCompany() {
        Company company = new Company();
        company.setName("Company");
        company = companyService.save(company);
        Assert.assertNotNull(company);
        Assert.assertTrue(companyService.exist(company.getId()));
    }

//    @Test
    public void testFindCompanyById() {
        Assert.assertNotNull(companyService.findById(1));
    }

//    @Test
    public void testFindAllCompanies() {
        Assert.assertNotNull(companyService.findAll());
    }

//    @Test
    public void testDeleteCompany() {
        Company company = new Company();
        company.setName("Company_2");
        company = companyService.save(company);
        Assert.assertNotNull(company);
        Assert.assertTrue(companyService.exist(company.getId()));
        companyService.delete(company.getId());
        Assert.assertFalse(companyService.exist(company.getId()));
    }

//    @Test
    public void testExistCompany() {
        Assert.assertTrue(companyService.exist(1));
    }

//    @Test
    public void testFindCompanyByName() {
        Assert.assertNotNull(companyService.findByName("Company"));
    }

//    @Test
    public void testDeleteCompanyWithProject() {
        companyService.delete(1);
        Assert.assertFalse(companyService.exist(1));
    }

//    @Test
    public void testSaveProject() {
        Project project = new Project();
        project.setName("Project");
        project.setCompany(companyService.findById(1));
        project = projectService.save(project);
        Assert.assertNotNull(project);
        Assert.assertTrue(projectService.exist(project.getId()));
    }

//    @Test
    public void testFindProjectById() {
        Assert.assertNotNull(projectService.findById(1));
    }

//    @Test
    public void testFindAllProjects() {
        Assert.assertNotNull(projectService.findAll());
    }

//    @Test
    public void testDeleteProject() {
        Project project = new Project();
        project.setName("Project_2");
        project.setCompany(companyService.findById(1));
        project = projectService.save(project);
        Assert.assertNotNull(project);
        Assert.assertTrue(projectService.exist(project.getId()));
        projectService.delete(project.getId());
        Assert.assertFalse(projectService.exist(project.getId()));
    }

//    @Test
    public void testExistProject() {
        Assert.assertTrue(projectService.exist(1));
    }

//    @Test
    public void testFindProjectByName() {
        Assert.assertNotNull(projectService.findByName("Project"));
    }
}