package com.example.spring.boot.onetomany.repository;

import com.example.spring.boot.onetomany.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Arkady on 02.09.2015.
 */

public interface ProjectRepository extends JpaRepository<Project, Integer> {

    Project findByName(String name);
}