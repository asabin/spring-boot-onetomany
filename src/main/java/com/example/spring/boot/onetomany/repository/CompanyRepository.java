package com.example.spring.boot.onetomany.repository;

import com.example.spring.boot.onetomany.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Arkady on 02.09.2015.
 */

public interface CompanyRepository extends JpaRepository<Company, Integer> {

    Company findByName(String name);
}