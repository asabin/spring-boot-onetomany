package com.example.spring.boot.onetomany.entity;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by Arkady on 02.09.2015.
 */

@Entity
public class Project {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private int id;

    private String name;

    @ManyToOne
    private Company company;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}