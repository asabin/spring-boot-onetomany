package com.example.spring.boot.onetomany.service.impl;

import com.example.spring.boot.onetomany.entity.Project;
import com.example.spring.boot.onetomany.repository.ProjectRepository;
import com.example.spring.boot.onetomany.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Arkady on 02.09.2015.
 */

@Service
public class ProjectServiceServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public Project save(Project project) {
        return projectRepository.saveAndFlush(project);
    }

    @Override
    public Project findById(int id) {
        return projectRepository.findOne(id);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void delete(int id) {
        projectRepository.delete(id);
    }

    @Override
    public boolean exist(int id) {
        return projectRepository.exists(id);
    }

    @Override
    public Project findByName(String name) {
        return projectRepository.findByName(name);
    }
}