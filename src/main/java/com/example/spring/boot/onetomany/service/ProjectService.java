package com.example.spring.boot.onetomany.service;

import com.example.spring.boot.onetomany.entity.Project;

import java.util.List;

/**
 * Created by Arkady on 02.09.2015.
 */

public interface ProjectService {

    Project save(Project project);

    Project findById(int id);

    List<Project> findAll();

    void delete(int id);

    boolean exist(int id);

    Project findByName(String name);
}